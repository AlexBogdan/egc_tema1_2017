#include "Laborator3.h"

#include <vector>
#include <iostream>

#include <Core/Engine.h>
#include "Transform2D.h"
#include "Object2D.h"

using namespace std;

Laborator3::Laborator3()
{
}

Laborator3::~Laborator3()
{
}

float screenWidth;
float screenHeight;

bool miscaAstronautul = false;
float speed;
float speedX;
float speedY;
int directionX;
int directionY;

#define NUMAR_PLATFORME_MARGINE 14
#define NUMAR_ASTEROIZI_INITIAL 3
int platform_type[NUMAR_PLATFORME_MARGINE +1];

int numar_asteroid = 0;

void Laborator3::Init()
{
	glm::ivec2 resolution = window->GetResolution();
	screenWidth = window->GetResolution().x;
	screenHeight = window->GetResolution().y;

	auto camera = GetSceneCamera();
	camera->SetOrthographic(0, (float)resolution.x, 0, (float)resolution.y, 0.01f, 400);
	camera->SetPosition(glm::vec3(0, 0, 50));
	camera->SetRotation(glm::vec3(0, 0, 0));
	camera->Update();
	GetCameraInput()->SetActive(false);

	glm::vec3 corner = glm::vec3(0, 0, 0);
	float squareSide = 100;

	// compute coordinates of square center
	float cx = corner.x + squareSide / 2;
	float cy = corner.y + squareSide / 2;
	
	// initialize tx and ty (the translation steps)
	translateX = 0;
	translateY = 0;

	// initialize sx and sy (the scale factors)
	scaleX = 1;
	scaleY = 1;
	
	// initialize angularStep
	angularStep = 0;

	// Init character data
	characterX = screenWidth / 2;
	characterY = screenHeight / 2;
	characterAngular = 0;
	characterAngle = 0;
	speed = 300;
	speedX = 0;
	speedY = 0;

	characterShield = 0;
	characterAmmo = 0;

	// Cream Astronautul
	Mesh* astronaut = Object2D::CreateAstronaut("astronaut", corner , screenWidth / 32, glm::vec3(0, 1, 0), true);
	AddMeshToList(astronaut);

	// Cream platforme pe margine

	// Jos
	Mesh* platforma_out_1 = Object2D::CreatePlatformaStationare
		("platforma_out_1", corner + glm::vec3(0,0,0),
		screenWidth / 32 * 9, screenHeight / 24, glm::vec3(1, 0, 0), true);
	AddMeshToList(platforma_out_1);
	Mesh* platforma_out_2 = Object2D::CreatePlatformaReflexie
		("platforma_out_2", corner + glm::vec3(screenWidth / 32 * 9, 0, 0),
		screenWidth / 32 * 5, screenHeight / 24, glm::vec3(0, 0, 1), true);
	AddMeshToList(platforma_out_2);
	Mesh* platforma_out_3 = Object2D::CreatePlatformaStationare
		("platforma_out_3", corner + glm::vec3(screenWidth / 32 * 14, 0, 0),
		screenWidth / 32 * 18, screenHeight / 24, glm::vec3(1, 0, 0), true);
	AddMeshToList(platforma_out_3);

	// Dreapta
	Mesh* platforma_out_4 = Object2D::CreatePlatformaStationare
		("platforma_out_4", corner + glm::vec3(screenWidth - screenHeight / 24, 0, 0),
		screenHeight / 24, screenHeight / 24 * 9, glm::vec3(1, 0, 0), true);
	AddMeshToList(platforma_out_4);
	Mesh* platforma_out_5 = Object2D::CreatePlatformaReflexie
		("platforma_out_5", corner + glm::vec3(screenWidth - screenHeight / 24, screenHeight / 24 * 9, 0),
		screenHeight / 24, screenHeight / 24 * 6, glm::vec3(0, 0, 1), true);
	AddMeshToList(platforma_out_5);
	Mesh* platforma_out_6 = Object2D::CreatePlatformaStationare
		("platforma_out_6", corner + glm::vec3(screenWidth - screenHeight / 24, screenHeight/24 * 15, 0),
		screenHeight / 24, screenHeight / 24 * 9, glm::vec3(1, 0, 0), true);
	AddMeshToList(platforma_out_6);

	//Sus
	Mesh* platforma_out_7 = Object2D::CreatePlatformaStationare
		("platforma_out_7", corner + glm::vec3(screenWidth / 32 * 28, screenHeight / 24 * 23, 0),
		screenWidth / 32 * 4, screenHeight / 24, glm::vec3(1, 0, 0), true);
	AddMeshToList(platforma_out_7);
	Mesh* platforma_out_8 = Object2D::CreatePlatformaReflexie
		("platforma_out_8", corner + glm::vec3(screenWidth / 32 * 24, screenHeight / 24 * 23, 0),
		screenWidth / 32 * 4, screenHeight / 24, glm::vec3(0, 0, 1), true);
	AddMeshToList(platforma_out_8);
	Mesh* platforma_out_9 = Object2D::CreatePlatformaStationare
		("platforma_out_9", corner + glm::vec3(screenWidth / 32 * 14, screenHeight / 24 * 23, 0),
		screenWidth / 32 * 10, screenHeight / 24, glm::vec3(1, 0, 0), true);
	AddMeshToList(platforma_out_9);
	Mesh* platforma_out_10 = Object2D::CreatePlatformaReflexie
		("platforma_out_10", corner + glm::vec3(screenWidth / 32 * 8, screenHeight / 24 * 23, 0),
		screenWidth / 32 * 6, screenHeight / 24, glm::vec3(0, 0, 1), true);
	AddMeshToList(platforma_out_10);
	Mesh* platforma_out_11 = Object2D::CreatePlatformaStationare
		("platforma_out_11", corner + glm::vec3(0, screenHeight / 24 * 23, 0),
		screenWidth / 32 * 8, screenHeight / 24, glm::vec3(1, 0, 0), true);
	AddMeshToList(platforma_out_11);

//Stanga
	Mesh* platforma_out_12 = Object2D::CreatePlatformaStationare
		("platforma_out_12", corner + glm::vec3(0, screenHeight / 24 * 18, 0),
		screenHeight / 24, screenHeight / 24 * 6, glm::vec3(1, 0, 0), true);
	AddMeshToList(platforma_out_12);
	Mesh* platforma_out_13 = Object2D::CreatePlatformaReflexie
		("platforma_out_13", corner + glm::vec3(0, screenHeight / 24 * 15, 0),
		screenHeight / 24, screenHeight / 24 * 3, glm::vec3(0, 0, 1), true);
	AddMeshToList(platforma_out_13);
	Mesh* platforma_out_14 = Object2D::CreatePlatformaStationare
		("platforma_out_14", corner + glm::vec3(0, 0, 0),
		screenHeight / 24, screenHeight / 24 * 15, glm::vec3(1, 0, 0), true);
	AddMeshToList(platforma_out_14);

	platform_type[1] = 1;
	platform_type[2] = 2;
	platform_type[3] = 1;
	platform_type[4] = 1;
	platform_type[5] = 2;
	platform_type[6] = 1;
	platform_type[7] = 1;
	platform_type[8] = 2;
	platform_type[9] = 1;
	platform_type[10] = 2;
	platform_type[11] = 1;
	platform_type[12] = 1;
	platform_type[13] = 2;
	platform_type[14] = 1;

	// Sfarsit margine

	// Asteorizi

	asteroid_huge = Object2D::CreateAsteroid("asteroid_huge", corner, 100, true);
	asteroid_medium = Object2D::CreateAsteroid("asteroid_medium", corner, 50, true);
	asteroid_small = Object2D::CreateAsteroid("asteroid_small", corner, 25, true);

	add_asteroid("asteroid0", 100, screenWidth / 32 * 24, screenHeight / 24 * 8, (float)(std::rand() % 314) / 100, -1, -1);
	add_asteroid("asteroid1", 50, screenWidth / 32 * 10, screenHeight / 24 * 18, (float)(std::rand() % 314) / 100, 1, -1);
	add_asteroid("asteroid2", 25, screenWidth / 32 * 6, screenHeight / 24 * 12, (float)(std::rand() % 314) / 100, 1, 1);
	numar_asteroid = 3;

	// Cream armele

	shieldX = screenWidth / 32 * 30;
	shieldY = screenHeight / 24 * 22;
	Mesh* shield = Object2D::CreateCircle("shield", corner + glm::vec3(shieldX, shieldY, 0), 30, glm::vec3(0, 0.5f, 1), false);
	AddMeshToList(shield);
	shieldActive = true;

	// Meshe pentru armele active ale caracterului
	Mesh* characterShield = Object2D::CreateCircle("characterShield", corner, 30, glm::vec3(0, 0.5f, 1), false);
	AddMeshToList(characterShield);

	Mesh* frag = Object2D::CreateSquare("frag", corner, 10, glm::vec3(0, 1, 1));
	AddMeshToList(frag);

	Mesh* explosion = Object2D::CreateCircle("explosion", corner, 10, glm::vec3(1, 0, 0.5), false);
	AddMeshToList(explosion);

	Mesh* end_platform = Object2D::CreatePlatformaStationare
		("end_platform", corner + glm::vec3(screenWidth / 32 * 16, screenHeight / 24 * 18, 0),
		screenHeight / 24 * 4, screenHeight / 24, glm::vec3(1, 0, 1), true);
	AddMeshToList(end_platform);
}

void Laborator3::FrameStart()
{
	// clears the color buffer (using the previously set color) and depth buffer
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glm::ivec2 resolution = window->GetResolution();
	// sets the screen area where to draw
	glViewport(0, 0, resolution.x, resolution.y);
}

void Laborator3::add_asteroid(std::string name, float size, float px, float py, float angle, int dx, int dy) {
	if (size > 60) {
		asteroids[name] = asteroid_huge;
	}
	else if (size > 30) {
		asteroids[name] = asteroid_medium;
	}
	else {
		asteroids[name] = asteroid_small;
	}
	asteroidMatrices[name] = glm::mat3();
	asteroidsSizes[name] = size;
	asteroidsPositions[name] = glm::vec3(px, py, 0);
	asteroidsAngles[name] = size;
	miscaAsteroid[name] = true;
	asteroidDirX[name] = dx;
	asteroidDirY[name] = dy;
	asteroidRender[name] = true;
}

void Laborator3::clear_asteroid(std::string name) {
	asteroidRender[name] = false;
	/*asteroids.erase(name);
	asteroidMatrices.erase(name);
	asteroidsSizes.erase(name);
	asteroidsPositions.erase(name);
	asteroidsAngles.erase(name);
	miscaAsteroid.erase(name);
	asteroidDirX.erase(name);
	asteroidDirY.erase(name);*/
}

void Laborator3::Update(float deltaTimeSeconds)
{

	if (game_over == true) {
		return ;
	}

	// -=-=-=-=-=-=-=-=-=-=- Init -=-=-=-=-=-=-=-=-=-=-=

	// -=-=-=-=- Init Map -=-=-=-=-=-=-=-=


	// -=-=-=-=- Init astronaut -=-=-=-=-=-

	astronautMatrix = glm::mat3(1);


	// -=-=-=-=-=-=- Actions -=-=-=-=-=-=-=-=-=

	// Move Character

	// Verificam daca astronautul s-a lovit de o platforma
	if (miscaAstronautul == true) {
		characterX += deltaTimeSeconds * speedX * directionX;
		characterY += deltaTimeSeconds * speedY * directionY;

		Mesh* astro_current_location = Object2D::CreateAstronaut("astronaut_current",
				glm::vec3(characterX, characterY, 0), screenWidth / 32, glm::vec3(0, 1, 0), true);

		// Verificam daca am luat scutul
		if (shieldActive == true) {
			bool result = Object2D::intersectShapes(astro_current_location, meshes["shield"]);
			if (result == true) {
				cout << "Am atins scutul";
				shieldActive = false;
				shieldRespawn = 20;

				characterShield = 4;
			}
		}

		if (Object2D::intersectShapes(astro_current_location, meshes["end_platform"])) {
			game_over = true;
		}

		// verificam daca avem coliziune cu o platforma
		for (int i = 1; i <= NUMAR_PLATFORME_MARGINE; i++) {
			string platforma = "platforma_out_";
			bool result = Object2D::intersectShapes(astro_current_location, meshes[platforma + to_string(i)]);
			if (result == true) {
				int second_object_position = Object2D::get_relative_position(astro_current_location, meshes[platforma + to_string(i)]);

				if (platform_type[i] == 2) {
					// Platofrma de stationare
					miscaAstronautul = false;
					switch (second_object_position) {
					case 1:
						characterAngle = 0;
						break;
					case 2:
						characterAngle = 3.14 / 2;
						break;
					case 3:
						characterAngle = 3.14;
						break;
					case 4:
						characterAngle = 3 * 3.14 / 2;
						break;
					}
				}
				else if (platform_type[i] == 1) {
					// Platforma de reflexie
					switch (second_object_position) {
					case 1:
						directionX *= -1;
						characterAngle = 3.14 - characterAngle;
						break;
					case 2:
						directionY *= -1;
						characterAngle = 2 * 3.14 - characterAngle;
						break;
					case 3:
						directionX *= -1;
						characterAngle = 3.14 - characterAngle;
						break;
					case 4:
						directionY *= -1;
						characterAngle = 2 * 3.14 - characterAngle;
						break;
					}
				}
			}
		}

		// Verificam daca avem coliziune cu un asteroid
		for (auto asteroid : asteroids) {
			if (asteroidRender[asteroid.first] == false) {
				continue;
			}

			Mesh* asteroid_temp = Object2D::CreateAsteroid("asteroid_temp", asteroidsPositions[asteroid.first], asteroidsSizes[asteroid.first], true);
			bool result = Object2D::intersectShapes(astro_current_location, asteroid_temp);

			if (result == true) {
				if (characterShield <= 0) {
					game_over = true;
					break;
				}

				if (asteroidsSizes[asteroid.first] <= 30) {
					clear_asteroid(asteroid.first);
				}

				directionX *= -1;
				directionY *= -1;
				characterShield--;
			}
		}
	}

	astronautMatrix *= Transform2D::Translate(characterX, characterY);
	astronautMatrix *= Transform2D::Rotate(characterAngle - 3.14 / 6);

	// Update shield

	if (characterShield > 0) {
		chracterShieldMatrix = glm::mat3(1) * Transform2D::Translate(characterX, characterY);
		RenderMesh2D(meshes["characterShield"], shaders["VertexColor"], chracterShieldMatrix);
	}
	

	// Move and check asteroids
	for (auto asteroid : asteroids) {
		if (asteroidRender[asteroid.first] == false) {
			continue;
		}
		if (miscaAsteroid[asteroid.first] == true) {
			asteroidsPositions[asteroid.first] +=
				glm::vec3(deltaTimeSeconds * (speed / asteroidsSizes[asteroid.first] * 10) * asteroidDirX[asteroid.first],
						deltaTimeSeconds * (speed / asteroidsSizes[asteroid.first] * 10) * asteroidDirY[asteroid.first], 0);
			asteroidsAngles[asteroid.first] += deltaTimeSeconds * 15 / asteroidsSizes[asteroid.first];

			Mesh* asteroid_temp = Object2D::CreateAsteroid("asteroid_temp", asteroidsPositions[asteroid.first],
				asteroidsSizes[asteroid.first], true);

			for (int i = 1; i <= NUMAR_PLATFORME_MARGINE; i++) {
				string platforma = "platforma_out_";
				bool result = Object2D::intersectShapes(asteroid_temp, meshes[platforma + to_string(i)]);
				if (result == true) {
					int second_object_position = Object2D::get_relative_position(asteroid_temp, meshes[platforma + to_string(i)]);

					switch (second_object_position) {
					case 1:
						asteroidDirX[asteroid.first] *= -1;
						break;
					case 2:
						asteroidDirY[asteroid.first] *= -1;
						break;
					case 3:
						asteroidDirX[asteroid.first] *= -1;
						break;
					case 4:
						asteroidDirY[asteroid.first] *= -1;
						break;
					}
				}
			}

			// Frag
			Mesh* frag = Object2D::CreateSquare("frag", glm::vec3(fragX, fragY, 0), 10, glm::vec3(0, 1, 1));
			if (fragActive == true && Object2D::intersectShapes(asteroid_temp, frag)) {
				// Explosion
				clear_asteroid(asteroid.first);

				if (asteroidsSizes[asteroid.first] > 30) {
					add_asteroid("asteroid" + to_string(numar_asteroid), asteroidsSizes[asteroid.first] / 2,
						asteroidsPositions[asteroid.first].x, asteroidsPositions[asteroid.first].y,
						asteroidsAngles[asteroid.first] - 1.4, asteroidDirX[asteroid.first] * -1, 0);
					numar_asteroid++;
					add_asteroid("asteroid" + to_string(numar_asteroid), asteroidsSizes[asteroid.first] / 2,
						asteroidsPositions[asteroid.first].x, asteroidsPositions[asteroid.first].y,
						asteroidsAngles[asteroid.first] - 1.4, 0, asteroidDirY[asteroid.first] * -1);
					numar_asteroid++;
				}

				fragActive = false;
				fragExplosionTime = 1;
			}
		}

		asteroidMatrices[asteroid.first] = glm::mat3(1);
		asteroidMatrices[asteroid.first] *= Transform2D::Translate(asteroidsPositions[asteroid.first].x, asteroidsPositions[asteroid.first].y);
		asteroidMatrices[asteroid.first] *= Transform2D::Rotate(asteroidsAngles[asteroid.first]);
	}

	if (fragActive == true) {
		RenderMesh2D(meshes["frag"], shaders["VertexColor"], Transform2D::Translate(fragX, fragY));
	}

	if (fragExplosionTime > 0) {
		explosion = Transform2D::Translate(fragX + 3, fragY + 3);
		explosion *= Transform2D::Scale(5 * (1 - fragExplosionTime), 5 * (1 - fragExplosionTime));
		fragExplosionTime -= deltaTimeSeconds;
		RenderMesh2D(meshes["explosion"], shaders["VertexColor"], explosion);
	}

	// -=-=-=-=-=-= Render -=-=-=-=--=-=

	// -=-=-=- Render Map -=-=-=-=-=-
	 
	// -=-= Render borders -=-=-=-
	RenderMesh2D(meshes["platforma_out_1"], shaders["VertexColor"], platforma_out_1Matrix);
	RenderMesh2D(meshes["platforma_out_2"], shaders["VertexColor"], platforma_out_2Matrix);
	RenderMesh2D(meshes["platforma_out_3"], shaders["VertexColor"], platforma_out_3Matrix);
	RenderMesh2D(meshes["platforma_out_4"], shaders["VertexColor"], platforma_out_4Matrix);
	RenderMesh2D(meshes["platforma_out_5"], shaders["VertexColor"], platforma_out_5Matrix);
	RenderMesh2D(meshes["platforma_out_6"], shaders["VertexColor"], platforma_out_6Matrix);
	RenderMesh2D(meshes["platforma_out_7"], shaders["VertexColor"], platforma_out_7Matrix);
	RenderMesh2D(meshes["platforma_out_8"], shaders["VertexColor"], platforma_out_8Matrix);
	RenderMesh2D(meshes["platforma_out_9"], shaders["VertexColor"], platforma_out_9Matrix);
	RenderMesh2D(meshes["platforma_out_10"], shaders["VertexColor"], platforma_out_10Matrix);
	RenderMesh2D(meshes["platforma_out_11"], shaders["VertexColor"], platforma_out_11Matrix);
	RenderMesh2D(meshes["platforma_out_12"], shaders["VertexColor"], platforma_out_12Matrix);
	RenderMesh2D(meshes["platforma_out_13"], shaders["VertexColor"], platforma_out_13Matrix);
	RenderMesh2D(meshes["platforma_out_14"], shaders["VertexColor"], platforma_out_14Matrix);

	//Platforma finish

	RenderMesh2D(meshes["end_platform"], shaders["VertexColor"], glm::mat3(1));

	// Render Asteroids

	for (auto asteroid : asteroids) {
		if (asteroidRender[asteroid.first] == true) {
			RenderMesh2D(asteroid.second, shaders["VertexColor"], asteroidMatrices[asteroid.first]);
		}
	}

	// Render Character
	RenderMesh2D(meshes["astronaut"], shaders["VertexColor"], astronautMatrix);

	// Render power-ups

	if (shieldActive == true) {
		RenderMesh2D(meshes["shield"], shaders["VertexColor"], shieldMatrix);
	}
	else {
		if (shieldRespawn < 0) {
			shieldActive = true;
		}
		else {
			shieldRespawn -= deltaTimeSeconds;
		}
	}

	// TODO: update steps for translation, rotation, scale, in order to create animations
	modelMatrix1 = glm::mat3(1);
	modelMatrix1 *= Transform2D::Translate(150, 250);

	translateX += deltaTimeSeconds * 20;
	translateY += deltaTimeSeconds * 20;
	modelMatrix1 *= Transform2D::Translate(translateX, translateY);
	// TODO: create animations by multiplying current transform matrix with matrices from Transform 2D

	RenderMesh2D(meshes["square1"], shaders["VertexColor"], modelMatrix1);

	modelMatrix2 = glm::mat3(1);
	modelMatrix2 *= Transform2D::Translate(400, 250);
	scaleX += deltaTimeSeconds;
	scaleY += deltaTimeSeconds;
	//modelMatrix2 *= Transform2D::Scale(scaleX, scaleY);
	//TODO create animations by multiplying current transform matrix with matrices from Transform 2D
	
	//RenderMesh2D(meshes["square2"], shaders["VertexColor"], modelMatrix2);

	modelMatrix3 = glm::mat3(1);
	//modelMatrix3 *= Transform2D::Translate(150+50, 250+50);
	angularStep += 3*deltaTimeSeconds;
	modelMatrix3 *= Transform2D::Translate(translateX, translateY);
	modelMatrix3 *= Transform2D::Rotate(angularStep);
	modelMatrix3 *= Transform2D::Translate(50, 50);
	
	//TODO create animations by multiplying current transform matrix with matrices from Transform 2D
	RenderMesh2D(meshes["square3"], shaders["VertexColor"], modelMatrix3);
}

void Laborator3::FrameEnd()
{

}

void Laborator3::OnInputUpdate(float deltaTime, int mods)
{
	
}

void Laborator3::OnKeyPress(int key, int mods)
{
	if (key == GLFW_KEY_G) {
		fragActive = true;
		fragX = characterX;
		fragY = characterY;
	}
}

void Laborator3::OnKeyRelease(int key, int mods)
{
	// add key release event
}

void Laborator3::OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY)
{
	// add mouse move event
}

void Laborator3::OnMouseBtnPress(int mouseX, int mouseY, int button, int mods)
{
	if (miscaAstronautul == true) {
		return;
	}
	miscaAstronautul = true;
	mouseY = screenHeight - mouseY; // Setam (0,0) coltul din stanga jos

	characterAngle = atan2(mouseY - characterY, mouseX - characterX);
	//cout << "(" << characterX << " , " << characterY << " , " << " --- "
	//	<< "(" << mouseX << " , " << mouseY << ")" << endl;
	float m = abs((mouseX - characterX) / (mouseY - characterY));

	speedX = (m / (m + 1)) * speed;
	speedY = (1 / (m + 1)) * speed;

	if (mouseX - characterX < 0) {
		directionX = -1;
	}
	else {
		directionX = 1;
	}

	if (mouseY - characterY < 0) {
		directionY = -1;
	}
	else {
		directionY = 1;
	}
}

void Laborator3::OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods)
{
	// add mouse button release event
}

void Laborator3::OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY)
{
}

void Laborator3::OnWindowResize(int width, int height)
{
}
