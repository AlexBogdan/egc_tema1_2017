#pragma once

#include <Component/SimpleScene.h>
#include <string>
#include <Core/Engine.h>

class Laborator3 : public SimpleScene
{
	public:
		Laborator3();
		~Laborator3();

		void Init() override;

	private:
		void FrameStart() override;
		void Update(float deltaTimeSeconds) override;
		void FrameEnd() override;

		void OnInputUpdate(float deltaTime, int mods) override;
		void OnKeyPress(int key, int mods) override;
		void OnKeyRelease(int key, int mods) override;
		void OnMouseMove(int mouseX, int mouseY, int deltaX, int deltaY) override;
		void OnMouseBtnPress(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseBtnRelease(int mouseX, int mouseY, int button, int mods) override;
		void OnMouseScroll(int mouseX, int mouseY, int offsetX, int offsetY) override;
		void OnWindowResize(int width, int height) override;

		void clear_asteroid(std::string name);
		void add_asteroid(std::string name, float size, float px, float py, float angle, int dx, int dy);

	protected:
		glm::mat3 platforma_out_1Matrix;
		glm::mat3 platforma_out_2Matrix;
		glm::mat3 platforma_out_3Matrix;
		glm::mat3 platforma_out_4Matrix;
		glm::mat3 platforma_out_5Matrix;
		glm::mat3 platforma_out_6Matrix;
		glm::mat3 platforma_out_7Matrix;
		glm::mat3 platforma_out_8Matrix;
		glm::mat3 platforma_out_9Matrix;
		glm::mat3 platforma_out_10Matrix;
		glm::mat3 platforma_out_11Matrix;
		glm::mat3 platforma_out_12Matrix;
		glm::mat3 platforma_out_13Matrix;
		glm::mat3 platforma_out_14Matrix;

		std::unordered_map<std::string, Mesh*> asteroids;
		std::unordered_map<std::string, glm::mat3> asteroidMatrices;
		std::unordered_map<std::string, float> asteroidsSizes;
		std::unordered_map<std::string, float> asteroidsAngles;
		std::unordered_map<std::string, glm::vec3> asteroidsPositions;
		std::unordered_map<std::string, float> asteroidsScale;
		std::unordered_map<std::string, bool> miscaAsteroid;
		std::unordered_map<std::string, int> asteroidDirX;
		std::unordered_map<std::string, int> asteroidDirY;
		std::unordered_map<std::string, bool> asteroidRender;

		Mesh *asteroid_huge, *asteroid_medium, *asteroid_small;

		glm::mat3 modelMatrix1;
		glm::mat3 modelMatrix2;
		glm::mat3 modelMatrix3;

		float translateX, translateY;
		float scaleX, scaleY;
		float angularStep;
		
		glm::mat3 astronautMatrix;
		glm::mat3 chracterShieldMatrix;
		//glm::mat3 characterAmmo;

		float characterX, characterY;
		float characterAngular;
		float characterAngle;
		float characterShield = 0;
		float characterAmmo = 0;
		float characterFrags = 0, fragExplosionTime, fragX, fragY, fragActive = false;
		glm::mat3 explosion;

		glm::mat3 shieldMatrix;
		float shieldX, shieldY;
		bool shieldActive;
		float shieldRespawn;

		bool renderEnd = false;
		bool game_over = false;
};
