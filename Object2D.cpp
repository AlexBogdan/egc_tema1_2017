#include "Object2D.h"

#include <Core/Engine.h>

Mesh* Object2D::CreateSquare(std::string name, glm::vec3 leftBottomCorner, float length, glm::vec3 color, bool fill)
{
	glm::vec3 corner = leftBottomCorner;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(corner, color),
		VertexFormat(corner + glm::vec3(length, 0, 0), color),
		VertexFormat(corner + glm::vec3(length, length, 0), color),
		VertexFormat(corner + glm::vec3(0, length, 0), color)
	};

	Mesh* square = new Mesh(name);
	std::vector<unsigned short> indices = { 0, 1, 2, 3 };
	
	if (!fill) {
		square->SetDrawMode(GL_LINE_LOOP);
	}
	else {
		// draw 2 triangles. Add the remaining 2 indices
		indices.push_back(0);
		indices.push_back(2);
	}

	square->InitFromData(vertices, indices);
	return square;
}

Mesh* Object2D::CreateAstronaut(std::string name, glm::vec3 center, float length, glm::vec3 color, bool fill)
{
	float height = length * sqrt(3) / 2;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(center + glm::vec3(0, - height * 2 / 3, 0), color + glm::vec3(1, -0.8, 0.5)),
		VertexFormat(center + glm::vec3(- length / 2, height * 1 / 3, 0), color + glm::vec3(1, -0.8, 0.5)),
		VertexFormat(center + glm::vec3(+ length / 2, height * 1 / 3, 0), color + glm::vec3(0, 0, 0))
	};

	Mesh* triangle = new Mesh(name);
	std::vector<unsigned short> indices = { 0, 1, 2};

	if (!fill) {
		triangle->SetDrawMode(GL_LINE_LOOP);
	}
	else {
		// draw 2 triangles. Add the remaining 2 indices
		indices.push_back(0);
		indices.push_back(2);
	}

	triangle->InitFromData(vertices, indices);
	return triangle;
}

Mesh* Object2D::CreatePlatformaStationare(std::string name, glm::vec3 leftBottomCorner, float length, float height, glm::vec3 color, bool fill)
{
	glm::vec3 corner = leftBottomCorner;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(corner, color),
		VertexFormat(corner + glm::vec3(length, 0, 0), color),
		VertexFormat(corner + glm::vec3(length, height, 0), color),
		VertexFormat(corner + glm::vec3(0, height, 0), color)
	};

	Mesh* platform = new Mesh(name);
	std::vector<unsigned short> indices = { 0, 1, 2, 3 };

	if (!fill) {
		platform->SetDrawMode(GL_LINE_LOOP);
	}
	else {
		// draw 2 triangles. Add the remaining 2 indices
		indices.push_back(0);
		indices.push_back(2);
	}

	platform->InitFromData(vertices, indices);
	return platform;
}

Mesh* Object2D::CreatePlatformaReflexie(std::string name, glm::vec3 leftBottomCorner, float length, float height, glm::vec3 color, bool fill)
{
	glm::vec3 corner = leftBottomCorner;

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(corner, color),
		VertexFormat(corner + glm::vec3(length, 0, 0), color),
		VertexFormat(corner + glm::vec3(length, height, 0), color),
		VertexFormat(corner + glm::vec3(0, height, 0), color)
	};

	Mesh* platform = new Mesh(name);
	std::vector<unsigned short> indices = { 0, 1, 2, 3 };

	if (!fill) {
		platform->SetDrawMode(GL_LINE_LOOP);
	}
	else {
		// draw 2 triangles. Add the remaining 2 indices
		indices.push_back(0);
		indices.push_back(2);
	}

	platform->InitFromData(vertices, indices);
	return platform;
}

Mesh* Object2D::CreateAsteroid(std::string name, glm::vec3 center, float size, bool fill)
{
	glm::vec3 color(0, 0, 0);
	if (size > 60) {
		color = glm::vec3(0.5f, 0.5f, 0);
	}
	else if (size  > 30) {
		color = glm::vec3(0, 0.5f, 0.5f);
	}
	else {
		color = glm::vec3(0.5f, 0, 0.5f);
	}

	std::vector<VertexFormat> vertices =
	{
		VertexFormat(center, color),
		VertexFormat(center + glm::vec3(-size / 2, -size, 0), color),
		VertexFormat(center + glm::vec3(size / 2, -size, 0), color),
		VertexFormat(center + glm::vec3(size, -size / 2, 0), color),
		VertexFormat(center + glm::vec3(size, size / 2, 0), color),
		VertexFormat(center + glm::vec3(size / 2, size, 0), color),
		VertexFormat(center + glm::vec3(-size / 2, size, 0), color),
		VertexFormat(center + glm::vec3(-size, size / 2, 0), color),
		VertexFormat(center + glm::vec3(-size, -size / 2, 0), color)
	};

	Mesh* asteroid = new Mesh(name);
	std::vector<unsigned short> indices =
		{0, 1, 2,
		 0, 2, 3,
		 0, 3, 4, 
		 0, 4, 5,
		 0, 5, 6,
		 0, 6, 7,
		 0, 7, 8,
		 0, 8, 1};

	if (!fill) {
		asteroid->SetDrawMode(GL_QUAD_STRIP);
	}
	else {
		// draw 2 triangles. Add the remaining 2 indices
		indices.push_back(0);
		indices.push_back(7);
	}

	asteroid->InitFromData(vertices, indices);
	return asteroid;
}

Mesh* Object2D::CreateCircle(std::string name, glm::vec3 center, float radius, glm::vec3 color, bool fill)
{
	std::vector<VertexFormat> vertices;

	for (int i = 0; i < 200; i++) {
		vertices.push_back(VertexFormat(center + glm::vec3(cos(i) * radius, sin(i) * radius, 0), color));
	}

	Mesh* circle = new Mesh(name);
	std::vector<unsigned short> indices;
	for (int i = 0; i < 200; i++) {
		indices.push_back(i);
	}
	

	if (!fill) {
		circle->SetDrawMode(GL_LINE_LOOP);
	}
	else {
		// draw 2 triangles. Add the remaining 2 indices
		indices.push_back(0);
		indices.push_back(200);
	}

	circle->InitFromData(vertices, indices);
	return circle;
}

bool intersects(glm::vec2 a1, glm::vec2 a2, glm::vec2 b1, glm::vec2 b2) {

	float m1 = (a2.y - a1.y) / (a2.x - a1.x);
	float m2 = (b2.y - b1.y) / (b2.x - b1.x);

	float y1 = -m1 * a1.x + a1.y;
	float y2 = -m2 * b1.x + b1.y;

	float x = (y2 - y1) / (m1 - m2);
	float y = m1 * (x - a1.x) + a1.y;

	float eps = 0;

	if (!(min(a1.x, a2.x) - eps <= x && x <= max(a1.x, a2.x) + eps)) {
		return false;
	}
	if (!(min(b1.x, b2.x) - eps <= x && x <= max(b1.x, b2.x) + eps)) {
		return false;
	}
	if (!(min(a1.y, a2.y) - eps <= y && y <= max(a1.y, a2.y) + eps)) {
		return false;
	}
	if (!(min(b1.y, b2.y) - eps <= y && y <= max(b1.y, b2.y) + eps)) {
		return false;
	}

	return true;
}

// Verificam daca caracterul a atins o platforma (de orice fel)
bool Object2D::intersectShapes(Mesh* shape1, Mesh* shape2) {
	int i, j;
	for (i = 0; i < shape1->vertices.size() - 1; i++) {
		glm::vec2 a1(shape1->vertices[i].position.x, shape1->vertices[i].position.y);
		glm::vec2 a2(shape1->vertices[i + 1].position.x, shape1->vertices[i + 1].position.y);

		for (j = 0; j < shape2->vertices.size() - 1; j++) {
			glm::vec2 b1(shape2->vertices[j].position.x, shape2->vertices[j].position.y);
			glm::vec2 b2(shape2->vertices[j + 1].position.x, shape2->vertices[j + 1].position.y);
			if (intersects(a1, a2, b1, b2)) {
				return true;
			}
		}
		glm::vec2 b1(shape2->vertices[j - 1].position.x, shape2->vertices[j - 1].position.y);
		glm::vec2 b2(shape2->vertices[0].position.x, shape2->vertices[0].position.y);
		if (intersects(a1, a2, b1, b2)) {
			return true;
		}
	}

	glm::vec2 a1(shape1->vertices[i - 1].position.x, shape1->vertices[i - 1].position.y);
	glm::vec2 a2(shape1->vertices[0].position.x, shape1->vertices[0].position.y);

	for (j = 0; j < shape2->vertices.size() - 1; j++) {
		glm::vec2 b1(shape2->vertices[j].position.x, shape2->vertices[j].position.y);
		glm::vec2 b2(shape2->vertices[j + 1].position.x, shape2->vertices[j + 1].position.y);
		if (intersects(a1, a2, b1, b2)) {
			return true;
		}
	}
	glm::vec2 b1(shape2->vertices[j - 1].position.x, shape2->vertices[j - 1].position.y);
	glm::vec2 b2(shape2->vertices[0].position.x, shape2->vertices[0].position.y);
	if (intersects(a1, a2, b1, b2)) {
		return true;
	}

	return false;
}

// 1 for left, 3 for right
int get_x_position(int x1, int x2) {
	if (x1 <= x2) {
		return 3;
	}
	else {
		return 1;
	}
}

// 2 for down, 4 for up
int get_y_position(int y1, int y2) {
	if (y1 <= y2) {
		return 4;
	}
	else {
		return 2;
	}
}

// return = 1 (left) | 2 (down) | 3 (right) | 4 (up)
int Object2D::get_relative_position(Mesh* object1, Mesh* object2) {
	int cnt_left = 0;
	int cnt_right = 0;
	int cnt_down = 0;
	int cnt_up = 0;

	for (int i = 0; i < object1->vertices.size(); i++) {
		int x1 = object1->vertices[i].position.x;
		int y1 = object1->vertices[i].position.y;

		for (int i = 0; i < object2->vertices.size(); i++) {
			int x2 = object2->vertices[i].position.x;
			int y2 = object2->vertices[i].position.y;

			if (get_x_position(x1, x2) == 1) {
				cnt_left++;
			}
			else {
				cnt_right++;
			}

			if (get_y_position(y1, y2) == 2) {
				cnt_down++;
			}
			else {
				cnt_up++;
			}
		}
	}

	//printf("Stanga = %d\n", cnt_left);
	//printf("Jos = %d\n", cnt_down);
	//printf("Dreapta = %d\n", cnt_right);
	//printf("Sus = %d\n", cnt_up);

	if (max(max(cnt_left, cnt_right), max(cnt_down, cnt_up)) == cnt_left) {
		return 1;
	}
	if (max(max(cnt_left, cnt_right), max(cnt_down, cnt_up)) == cnt_down) {
		return 2;
	}
	if (max(max(cnt_left, cnt_right), max(cnt_down, cnt_up)) == cnt_right) {
		return 3;
	}
	if (max(max(cnt_left, cnt_right), max(cnt_down, cnt_up)) == cnt_up) {
		return 4;
	}

	return 0;
}