#pragma once

#include <string>

#include <include/glm.h>
#include <Core/GPU/Mesh.h>

namespace Object2D
{

	// Create square with given bottom left corner, length and color
	Mesh* CreateSquare(std::string name, glm::vec3 leftBottomCorner, float length, glm::vec3 color, bool fill = false);

	// Create character with given bottom left corner, length and color
	Mesh* CreateAstronaut(std::string name, glm::vec3 center, float length, glm::vec3 color, bool fill = false);

	// Create platform with given bottom left corner, length, height and color
	Mesh* CreatePlatformaStationare(std::string name, glm::vec3 leftBottomCorner, float length, float height, glm::vec3 color, bool fill = false);

	// Create platform with given bottom left corner, length, height and color
	Mesh* CreatePlatformaReflexie(std::string name, glm::vec3 leftBottomCorner, float length, float height, glm::vec3 color, bool fill = false);
	
	// Create asteroid with given center and size
	Mesh* CreateAsteroid(std::string name, glm::vec3 center, float size, bool fill);

	// Create circle with given center and radius
	Mesh* CreateCircle(std::string name, glm::vec3 center, float size, glm::vec3 color, bool fill);

	bool intersectShapes(Mesh* shape1, Mesh* shape2);

	int get_relative_position(Mesh* object1, Mesh* object2);

}

