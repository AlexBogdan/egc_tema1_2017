-=-=-=-=-=-= Andrei Bogdan Alexandru -=-=-=-=-=-=
=-=-=-=-=-=-=-=-=- 336 CB -=-=-=-=-=-=-=-=-=-=-=-
-=-=-=-=-=-=-=-= Tema 1 EGC -=-=-=-=-=-=-=-=-=-=-

Tema a fost impementata pornind de la codul laboratorului 3.

Pentru a implementa tema am ales sa creez functii separate pentru fiecare
forma (platforme, asteroizi, astronaut) care sa returneze meshe pentru
acestea.

Prin functiile de Translate, Rotate si Scale manipulam obiectele din scena.
Coliziunile sunt trate prin verificarea unei eventuale intersectii ale
laturilor dintre cele 2 meshe verificate. Verificarea se face verificand
latura cu latura.

O parte dintre obiectele create sunt pastrate in map-uri, iar adaugarea si
scoaterea lor se face in mod automat in timpul jocului, spre exemplu crearea
asteroizilor (cand unul este distrus si apar altii mai mici).