#pragma once

#include <include/glm.h>

namespace Transform2D
{
	// Translate matrix
	inline glm::mat3 Translate(float tx, float ty)
	{
		return glm::transpose(
			glm::mat3(1, 0, tx,
				0, 1, ty,
				0, 0, 1)
		);
	}

	// Scale matrix
	inline glm::mat3 Scale(float sx, float sy)
	{
		return glm::transpose(
			glm::mat3(sx, 0, 0,
				0, sy, 0,
				0, 0, 1)
		);
	}

	// Rotate matrix
	inline glm::mat3 Rotate(float u)
	{
		return glm::transpose(
			glm::mat3(cos(u), -sin(u), 0,
				sin(u), cos(u), 0,
				0, 0, 1)
		);
	}

	inline void RotateMesh(Mesh* mesh, float u) {
		for (int i = 0; i < mesh->vertices.size(); i++) {	
			int x = mesh->vertices[i].position.x;
			int y = mesh->vertices[i].position.y;

			mesh->vertices[i].position.x = cos(u) * x - sin(u) * y;
			mesh->vertices[i].position.y = sin(u) * x + cos(u) * y;
		}
	}

	inline void TranslateMesh(Mesh* mesh, float tx, float ty) {
		for (int i = 0; i < mesh->vertices.size(); i++) {
			mesh->vertices[i].position.x += tx;
			mesh->vertices[i].position.y += ty;
		}
	}

	inline void ScaleMesh(Mesh* mesh, float sx, float sy) {
		for (int i = 0; i < mesh->vertices.size(); i++) {
			mesh->vertices[i].position.x *= sx;
			mesh->vertices[i].position.y *= sy;
		}
	}
}
